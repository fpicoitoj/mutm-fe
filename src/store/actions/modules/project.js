import projectAPI from "./../../../lib/api/modules/project";
import {
  DELETE_PROJECT,
  FETCH_PROJECTS,
  UNCOMPLETE_TASK,
  COMPLETE_TASK,
  ADD_TASK,
  EDIT_PROJECT,
  CREATE_PROJECT,
  DELETE_TASK
} from "./../../types";

export const fetchProjects = () => async dispatch => {
  const { projects } = await projectAPI.getAll();

  dispatch({
    type: FETCH_PROJECTS,
    payload: projects
  });
};

export const createProject = ({ name }) => async dispatch => {
  const project = await projectAPI.create({ name });

  dispatch({
    type: CREATE_PROJECT,
    payload: project
  });
};

export const editProject = (projectId, { name }) => async dispatch => {
  const project = await projectAPI.update(projectId, { name });

  dispatch({
    type: EDIT_PROJECT,
    payload: project
  });
};

export const deleteProject = projectId => async dispatch => {
  const project = await projectAPI.delete(projectId);

  dispatch({
    type: DELETE_PROJECT,
    payload: project
  });
};

export const addTask = (projectId, { description }) => async dispatch => {
  const task = await projectAPI.addTask(projectId, {
    description
  });

  dispatch({
    type: ADD_TASK,
    payload: {
      projectId,
      task
    }
  });
};

export const deleteTask = (projectId, id) => async dispatch => {
  const task = await projectAPI.deleteTask(projectId, id);

  dispatch({
    type: DELETE_TASK,
    payload: {
      projectId,
      task
    }
  });
};

export const completeTask = (projectId, id) => async dispatch => {
  const task = await projectAPI.completeTask(projectId, id);

  dispatch({
    type: COMPLETE_TASK,
    payload: {
      projectId,
      task
    }
  });
};

export const uncompleteTask = (projectId, id) => async dispatch => {
  const task = await projectAPI.uncompleteTask(projectId, id);

  dispatch({
    type: UNCOMPLETE_TASK,
    payload: {
      projectId,
      task
    }
  });
};
