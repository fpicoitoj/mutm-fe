import { SET_USER, UNSET_USER } from "./../../types";

export const setUser = ({ name, email }) => async dispatch => {
  dispatch({
    type: SET_USER,
    payload: {
      name,
      email
    }
  });
};

export const unsetUser = () => async dispatch => {
  dispatch({
    type: UNSET_USER
  });
};
