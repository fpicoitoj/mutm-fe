export const SET_USER = "SET_USER";
export const UNSET_USER = "UNSET_USER";

export const CREATE_PROJECT = "CREATE_PROJECT";
export const EDIT_PROJECT = "EDIT_PROJECT";
export const FETCH_PROJECTS = "FETCH_PROJECTS";
export const DELETE_PROJECT = "DELETE_PROJECT";

export const ADD_TASK = "ADD_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const COMPLETE_TASK = "COMPLETE_TASK";
export const UNCOMPLETE_TASK = "UNCOMPLETE_TASK";
