import { SET_USER, UNSET_USER } from "./../../types";

const INITIAL_STATE = {
  name: "",
  email: ""
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...INITIAL_STATE,
        ...action.payload
      };

    case UNSET_USER:
      return {
        ...INITIAL_STATE
      };

    default:
      return state;
  }
};
