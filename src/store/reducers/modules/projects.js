import {
  FETCH_PROJECTS,
  COMPLETE_TASK,
  UNCOMPLETE_TASK,
  ADD_TASK,
  DELETE_PROJECT,
  EDIT_PROJECT,
  CREATE_PROJECT,
  DELETE_TASK,
  UNSET_USER
} from "./../../types";

const INITIAL_STATE = {
  projects: null,
  isFetching: false
};

export default (state = INITIAL_STATE, action) => {
  let projects;

  switch (action.type) {
    case FETCH_PROJECTS:
      return {
        ...INITIAL_STATE,
        projects: [...action.payload]
      };

    case CREATE_PROJECT:
      return {
        ...INITIAL_STATE,
        projects: [...state.projects, action.payload]
      };

    case EDIT_PROJECT:
      return {
        ...INITIAL_STATE,
        projects: state.projects.map(f => {
          if (f._id === action.payload._id) {
            return {
              ...f,
              ...action.payload
            };
          }

          return f;
        })
      };

    case DELETE_PROJECT:
      return {
        ...INITIAL_STATE,
        projects: state.projects.filter(f => f._id !== action.payload._id)
      };

    case ADD_TASK:
      projects = state.projects.map(p => {
        if (p._id === action.payload.projectId) {
          return {
            ...p,
            tasks: [...p.tasks, action.payload.task]
          };
        }

        return p;
      });

      return {
        ...INITIAL_STATE,
        projects
      };

    case DELETE_TASK:
      projects = state.projects.map(p => {
        if (p._id === action.payload.projectId) {
          return {
            ...p,
            tasks: p.tasks.filter(f => f._id !== action.payload.task._id)
          };
        }

        return p;
      });

      return {
        ...INITIAL_STATE,
        projects
      };

    case COMPLETE_TASK:
    case UNCOMPLETE_TASK:
      projects = state.projects.map(p => {
        if (p._id === action.payload.projectId) {
          return {
            ...p,
            tasks: p.tasks.map(t => {
              if (t._id === action.payload.task._id) return action.payload.task;
              return t;
            })
          };
        }

        return p;
      });

      return {
        ...INITIAL_STATE,
        projects: [...projects]
      };

    case UNSET_USER:
      return {
        ...INITIAL_STATE
      };

    default:
      return state;
  }
};
