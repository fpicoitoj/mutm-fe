import { combineReducers } from "redux";
import userReducer from "./modules/user";
import projectsReducer from "./modules/projects";

const rootReducer = combineReducers({
  userReducer,
  projectsReducer
});

export default rootReducer;
