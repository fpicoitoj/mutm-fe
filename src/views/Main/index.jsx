import React, { Component, Fragment } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import Navbar from "./../../components/Navbar";
import { unsetUser } from "./../../store/actions";

class Main extends Component {
  constructor(props) {
    super(props);
    this.handleOnLogout = this.handleOnLogout.bind(this);
    this.handleOnCreate = this.handleOnCreate.bind(this);
  }

  // ### LIFECYCLE
  componentDidMount() {
    if (!localStorage.getItem("token")) return this.props.history.push("/auth");
  }

  componentDidUpdate() {
    if (this.props.user.name === "") return this.props.history.push("/auth");
  }

  // ### HANDLERS
  async handleOnLogout() {
    localStorage.removeItem("token");

    await this.props.unsetUser();

    this.props.history.push("/auth");
  }

  handleOnCreate() {
    this.props.history.push("/project/create");
  }

  render() {
    return (
      <Fragment>
        <Navbar
          name={this.props.user.name}
          onLogout={this.handleOnLogout}
          onCreate={this.handleOnCreate}
          history={this.props.history}
        />
        {this.props.user.name !== "" && (
          <div className="container">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/project" component={Home} />
              <Route path="*" component={NotFound} />
            </Switch>
          </div>
        )}
      </Fragment>
    );
  }
}

function mapStateToProps({ userReducer }) {
  return {
    user: userReducer
  };
}

export default connect(mapStateToProps, { unsetUser })(Main);
