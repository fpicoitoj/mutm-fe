import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import CreateButton from "./../../../../components/CreateButton";
import ProjectCard from "./../../../../components/ProjectCard";
import ProjectDeleteModal from "./../../../../components/ProjectDeleteModal";
import ProjectEditModal from "./../../../../components/ProjectEditModal";
import ProjectCreateModal from "./../../../../components/ProjectCreateModal";
import {
  fetchProjects,
  completeTask,
  uncompleteTask,
  deleteProject,
  editProject,
  createProject,
  deleteTask
} from "./../../../../store/actions";
import "./style.css";

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleOnDeleteTask = this.handleOnDeleteTask.bind(this);
    this.handleOnEdit = this.handleOnEdit.bind(this);
    this.handleOnDelete = this.handleOnDelete.bind(this);
    this.handleOnComplete = this.handleOnComplete.bind(this);
    this.handleOnUncomplete = this.handleOnUncomplete.bind(this);
    this.handleOnDeleteCancel = this.handleOnDeleteCancel.bind(this);
    this.handleOnDeleteConfirm = this.handleOnDeleteConfirm.bind(this);
    this.handleOnEditCancel = this.handleOnEditCancel.bind(this);
    this.handleOnEditConfirm = this.handleOnEditConfirm.bind(this);
    this.handleOnCreateCancel = this.handleOnCreateCancel.bind(this);
    this.handleOnCreateConfirm = this.handleOnCreateConfirm.bind(this);
  }

  async getAllProjects() {
    await this.props.fetchProjects();
  }

  componentDidMount() {
    this.getAllProjects();
  }

  handleOnEdit(id) {
    this.props.history.push(`/project/${id}/edit`);
  }

  handleOnDelete(id) {
    this.props.history.push(`/project/${id}/delete`);
  }

  handleOnComplete(projectId, id) {
    this.props.completeTask(projectId, id);
  }

  handleOnUncomplete(projectId, id) {
    this.props.uncompleteTask(projectId, id);
  }

  async handleOnDeleteConfirm(id) {
    await this.props.deleteProject(id);
    this.props.history.push("/");
  }

  handleOnDeleteCancel() {
    this.props.history.push("/");
  }

  async handleOnEditConfirm(id, payload) {
    await this.props.editProject(id, payload);
    this.props.history.push("/");
  }

  handleOnEditCancel() {
    this.props.history.push("/");
  }

  async handleOnCreateConfirm(payload) {
    await this.props.createProject(payload);
    this.props.history.push("/");
  }

  handleOnCreateCancel() {
    this.props.history.push("/");
  }

  async handleOnDeleteTask(projectId, id) {
    await this.props.deleteTask(projectId, id);
  }

  renderProjectCards() {
    if (this.props.projects.projects.length === 0)
      return (
        <div className="home__empty">
          <img
            src={require("./../../../../assets/undraw_chilling.svg")}
            alt="no projects"
          />
          <h1>You have no projects.</h1>
          <CreateButton title="Create a new one" history={this.props.history} />
        </div>
      );

    return this.props.projects.projects.map(p => (
      <ProjectCard
        key={p._id}
        {...p}
        onEdit={this.handleOnEdit}
        onDelete={this.handleOnDelete}
        onComplete={this.handleOnComplete}
        onUncomplete={this.handleOnUncomplete}
        onDeleteTask={this.handleOnDeleteTask}
      />
    ));
  }

  render() {
    if (this.props.projects.projects === null) {
      return (
        <div className="home__loading">
          <i className="fas fa-spinner fa-spin fa-3x" />
        </div>
      );
    }

    return (
      <div>
        {this.renderProjectCards()}
        <Route
          path="/project/:id/delete"
          render={routeProps => {
            const project = this.props.projects.projects.find(
              p => p._id === routeProps.match.params.id
            );

            if (!project) return <Redirect to="/" />;

            return (
              <ProjectDeleteModal
                isOpen={true}
                id={project._id}
                icon="fas fa-trash"
                title={project.name}
                message="Delete this project?"
                onConfirm={this.handleOnDeleteConfirm}
                onCancel={this.handleOnDeleteCancel}
              />
            );
          }}
        />
        <Route
          path="/project/:id/edit"
          render={routeProps => {
            const project = this.props.projects.projects.find(
              p => p._id === routeProps.match.params.id
            );

            if (!project) return <Redirect to="/" />;

            return (
              <ProjectEditModal
                isOpen={true}
                id={project._id}
                icon="fas fa-edit"
                title={project.name}
                value={project.name}
                onConfirm={this.handleOnEditConfirm}
                onCancel={this.handleOnEditCancel}
              />
            );
          }}
        />
        <Route
          path="/project/create"
          render={() => (
            <ProjectCreateModal
              isOpen={true}
              icon="fas fa-check"
              title="CREATE NEW PROJECT"
              onConfirm={this.handleOnCreateConfirm}
              onCancel={this.handleOnCreateCancel}
            />
          )}
        />
      </div>
    );
  }
}

function mapStateToProps({ projectsReducer }) {
  return {
    projects: projectsReducer
  };
}

export default connect(mapStateToProps, {
  fetchProjects,
  completeTask,
  uncompleteTask,
  deleteProject,
  editProject,
  createProject,
  deleteTask
})(Home);
