import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import authAPI from "./../../../../lib/api/modules/auth";
import { setUser } from "./../../../../store/actions";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isBusy: false,
      message: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  async handleSubmit(event) {
    event.preventDefault();

    this.setState({ isBusy: true });

    const login = await authAPI.login({
      email: this.state.email,
      password: this.state.password
    });

    this.setState({ isBusy: false });

    if (login.status === 500) return this.setState({ message: login.message });

    await this.props.setUser(login.user);

    this.props.history.push("/");
  }

  render() {
    return (
      <Fragment>
        <h3 className="title has-text-grey">Login</h3>
        <p className="subtitle has-text-grey">Please login to proceed.</p>
        <div className="box">
          <figure className="avatar">
            <img
              src={require("./../../../../assets/undraw_login.svg")}
              alt="multi user task manager logo"
            />
          </figure>
          <form onSubmit={this.handleSubmit}>
            <div className="field">
              <div className="control">
                <input
                  className="input"
                  type="email"
                  placeholder="Your Email"
                  autoFocus=""
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>
            </div>

            <div className="field">
              <div className="control">
                <input
                  className="input"
                  type="password"
                  placeholder="Your Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <button
              type="submit"
              className={`button is-block is-primary is-fullwidth ${
                this.state.isBusy ? "is-loading" : ""
              }`}
              disabled={this.state.isBusy}
            >
              Login
            </button>
            {this.state.message && (
              <article className="message is-danger">
                <div className="message-body">{this.state.message}</div>
              </article>
            )}
          </form>
        </div>
        <p className="has-text-grey">
          <Link to="/auth/register">Register</Link>
        </p>
      </Fragment>
    );
  }
}

export default connect(null, { setUser })(Login);
