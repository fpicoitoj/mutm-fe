import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import authAPI from "./../../../../lib/api/modules/auth";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      isBusy: false,
      message: "",
      isCreated: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  async handleSubmit(event) {
    event.preventDefault();

    this.setState({ isBusy: true });

    const register = await authAPI.register({
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    });

    this.setState({ isBusy: false });

    if (register.status === 500)
      return this.setState({ message: register.message });

    this.setState({ isCreated: true });
  }

  renderBody() {
    if (this.state.isCreated) {
      return (
        <article className="message is-success">
          <div className="message-body">User created! Login now!</div>
        </article>
      );
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="field">
          <div className="control">
            <input
              className="input is-large"
              type="text"
              placeholder="Your Name"
              autoFocus=""
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </div>
        </div>

        <div className="field">
          <div className="control">
            <input
              className="input"
              type="email"
              placeholder="Your Email"
              autoFocus=""
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>
        </div>

        <div className="field">
          <div className="control">
            <input
              className="input"
              type="password"
              placeholder="Your Password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <button
          type="submit"
          className={`button is-block is-primary is-fullwidth ${
            this.state.isBusy ? "is-loading" : ""
          }`}
          disabled={this.state.isBusy}
        >
          Register
        </button>
        {this.state.message && (
          <article className="message is-danger">
            <div className="message-body">{this.state.message}</div>
          </article>
        )}
      </form>
    );
  }

  render() {
    return (
      <Fragment>
        <h3 className="title has-text-grey">Register</h3>
        <p className="subtitle has-text-grey">Please register to proceed.</p>
        <div className="box">
          <figure className="avatar">
            <img
              src={require("./../../../../assets/undraw_fans.svg")}
              alt="multi user task manager logo"
            />
          </figure>
          {this.renderBody()}
        </div>
        <p className="has-text-grey">
          <Link to="/auth/login">Login</Link>
        </p>
      </Fragment>
    );
  }
}

export default Register;
