import React, { Component } from "react";
import { Switch, Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import Login from "./containers/Login";
import Register from "./containers/Register";
import "./style.css";

class Auth extends Component {
  // #todo - refactor this, also used in main
  verifyIfNotUser() {
    if (localStorage.getItem("token")) return this.props.history.push("/");
  }

  componentDidMount() {
    this.verifyIfNotUser();
  }

  render() {
    return (
      <div className="auth hero-body">
        <div className="container has-text-centered">
          <div className="column is-4 is-offset-4">
            <Switch>
              <Route path="/auth/login" component={Login} />
              <Route path="/auth/register" exact component={Register} />
              <Redirect to="/auth/login" />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ userReducer }) {
  return {
    user: userReducer
  };
}

export default connect(mapStateToProps, null)(Auth);
