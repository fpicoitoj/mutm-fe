import React, { Component } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Auth from "./views/Auth";
import Main from "./views/Main";
import authAPI from "./lib/api/modules/auth";
import { setUser } from "./store/actions";
import "./style.css";

class App extends Component {
  async verifyJWT() {
    if (localStorage.getItem("token")) {
      const verify = await authAPI.verify();

      if (verify.status === 500) {
        localStorage.removeItem("token");
        return this.props.history.push("/auth");
      }

      await this.props.setUser(verify);
    }
  }

  componentDidMount() {
    this.verifyJWT();
  }

  render() {
    return (
      <section className="App">
        <Switch>
          <Route path="/auth" component={Auth} />
          <Route path="*" component={Main} />
        </Switch>
      </section>
    );
  }
}

export default withRouter(connect(null, { setUser })(App));
