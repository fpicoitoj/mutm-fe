import React, { Component } from "react";
import Modal from "react-modal";
import "./style.css";

// https://github.com/reactjs/react-modal
class BaseModal extends Component {
  constructor(props) {
    super(props);

    this.handleOnClose = this.handleOnClose.bind(this);

    this.state = { isOpen: false };
  }

  componentWillMount() {
    this.setState({ isOpen: this.props.isOpen });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.state.isOpen) {
      this.setState({ isOpen: nextProps.isOpen });
    }
  }

  handleOnClose() {
    this.setState({ isOpen: false });

    if (this.props.onClose) this.props.onClose();
  }

  render() {
    const { icon, title, children } = this.props;

    return (
      <Modal
        isOpen={this.state.isOpen}
        shouldReturnFocusAfterClose={false}
        ariaHideApp={false}
        className={{
          base: "edi__modal",
          afterOpen: "edi__modal__after-open",
          beforeClose: "edi__modal__before-close"
        }}
        overlayClassName={{
          base: "edi__modal__overlay",
          afterOpen: "edi__modal__overlay__after-open",
          beforeClose: "edi__modal__overlay__before-close"
        }}
      >
        <div className="header">
          <div className="header-info">
            <i className={`${icon} fa-2x`} />
            <span>{title}</span>
          </div>
          <div className="close">
            <button
              onClick={this.handleOnClose}
              className="button modal-close-button is-rounded"
            >
              <i className="fas fa-times" />
            </button>
          </div>
        </div>
        <div className="content">{children}</div>
      </Modal>
    );
  }
}

export default BaseModal;
