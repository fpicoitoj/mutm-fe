import React from "react";
import { Link } from "react-router-dom";
import CreateButton from "./../CreateButton";
import "./style.css";

const Navbar = ({ name, onLogout, onCreate, history }) => {
  return (
    <nav className="main__navbar" aria-label="main navigation">
      <div className="container">
        <div className="navbar__logo">
          <Link to="/">
            <img src={require("./../../assets/clipboard.svg")} alt="logo" />
            <h1 className="title is-hidden-mobile">MUTM</h1>
          </Link>
        </div>

        <div className="navbar__left">
          <div className="field is-grouped">
            <p className="control">
              <CreateButton title="New" history={history} />
            </p>
          </div>
        </div>

        <div className="navbar__right">
          <div className="field is-grouped">
            <span className="navbar__username">{name}</span>
            <button className="button is-rounded" onClick={onLogout}>
              <span className="icon">
                <i className="fas fa-sign-out-alt" />
              </span>
            </button>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
