import React from "react";
import BaseModal from "./../BaseModal";

const ProjectDeleteModal = ({
  isOpen,
  id,
  icon,
  title,
  message,
  onConfirm,
  onCancel
}) => {
  return (
    <BaseModal isOpen={isOpen} onClose={onCancel} title={title} icon={icon}>
      <div className="modal__confirm">
        <label>{message}</label>
        <div className="modal__confirm__options">
          <button onClick={onCancel} className="button">
            <i className="fa fa-times" />
          </button>
          <button onClick={_ => onConfirm(id)} className="button is-danger">
            <i className="fa fa-check" />
          </button>
        </div>
      </div>
    </BaseModal>
  );
};

export default ProjectDeleteModal;
