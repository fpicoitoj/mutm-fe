import React, { Component } from "react";
import BaseModal from "./../BaseModal";

class ProjectEditModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.value,
      isBusy: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ isBusy: true });
    this.props.onConfirm(this.props.id, { name: this.state.name });
  }

  render() {
    const { isOpen, onCancel, title, icon } = this.props;

    return (
      <BaseModal isOpen={isOpen} onClose={onCancel} title={title} icon={icon}>
        <form className="modal__confirm" onSubmit={this.handleSubmit}>
          <div className="field">
            <label className="label">Name</label>
            <div className="control">
              <input
                className="input"
                type="text"
                placeholder="Text input"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="modal__confirm__options">
            <button type="button" onClick={onCancel} className="button">
              <i className="fa fa-times" />
            </button>
            <button
              type="submit"
              className={`button is-primary ${
                this.state.isBusy ? "is-loading" : ""
              }`}
              disabled={this.state.isBusy}
            >
              <i className="fa fa-check" />
            </button>
          </div>
        </form>
      </BaseModal>
    );
  }
}

export default ProjectEditModal;
