import React, { Component } from "react";
import BaseModal from "./../BaseModal";

class ProjectCreateModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      isBusy: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const { isOpen, onCancel, title, icon, onConfirm } = this.props;

    return (
      <BaseModal isOpen={isOpen} onClose={onCancel} title={title} icon={icon}>
        <div className="modal__confirm">
          <div className="field">
            <label className="label">Name</label>
            <div className="control">
              <input
                className="input"
                type="text"
                placeholder="Text input"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="modal__confirm__options">
            <button onClick={onCancel} className="button">
              <i className="fa fa-times" />
            </button>
            <button
              onClick={() => onConfirm({ name: this.state.name })}
              className="button is-primary"
            >
              <i className="fa fa-check" />
            </button>
          </div>
        </div>
      </BaseModal>
    );
  }
}

export default ProjectCreateModal;
