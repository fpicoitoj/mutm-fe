import React from "react";

const CreateButton = ({ title, history }) => {
  const onCreate = () => {
    return history.push("/project/create");
  };

  return (
    <button className="button is-primary is-rounded" onClick={onCreate}>
      <span className="icon">
        <i className="fas fa-plus" />
      </span>
      <span>{title}</span>
    </button>
  );
};

export default CreateButton;
