import React, { Component } from "react";
import RowBusyOverlay from "./../RowBusyOverlay";

class TodoRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBusy: false,
      overlayState: ""
    };

    this.handleOnDelete = this.handleOnDelete.bind(this);
    this.handleOnComplete = this.handleOnComplete.bind(this);
  }

  handleOnComplete(projectId, taskId) {
    this.setState({ isBusy: true, overlayState: "completing" });

    this.props.onComplete(projectId, taskId);
  }

  handleOnDelete(projectId, taskId) {
    this.setState({ isBusy: true, overlayState: "deleting" });

    this.props.onDelete(projectId, taskId);
  }

  render() {
    const { projectId, taskId, description } = this.props;

    return (
      <div className="panel-block pc__todo">
        <input
          type="checkbox"
          onChange={() => this.handleOnComplete(projectId, taskId)}
        />
        <span>{description}</span>
        <button
          type="button"
          className="button is-small"
          onClick={() => this.handleOnDelete(projectId, taskId)}
        >
          <i className="fas fa-trash" />
        </button>
        <RowBusyOverlay
          isVisible={this.state.isBusy}
          state={this.state.overlayState}
        />
      </div>
    );
  }
}

export default TodoRow;
