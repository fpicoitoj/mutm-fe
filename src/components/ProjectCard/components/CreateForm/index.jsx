import React, { Component } from "react";
import { connect } from "react-redux";
import { addTask } from "./../../../../store/actions";

class CreateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      isBusy: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  async handleSubmit(event) {
    event.preventDefault();

    this.setState({ isBusy: true });

    await this.props.addTask(this.props.projectId, {
      description: this.state.description
    });

    this.setState({ description: "", isBusy: false });
  }

  render() {
    return (
      <form
        className="field is-grouped card-footer-item"
        onSubmit={this.handleSubmit}
      >
        <p className="control is-expanded">
          <input
            className="input"
            type="text"
            placeholder="Create a task"
            name="description"
            value={this.state.description}
            onChange={this.handleChange}
          />
        </p>
        <button
          type="submit"
          className={`button control ${this.state.isBusy ? "is-loading" : ""}`}
          disabled={this.state.isBusy || this.state.description.length === 0}
        >
          Add
        </button>
      </form>
    );
  }
}

export default connect(null, { addTask })(CreateForm);
