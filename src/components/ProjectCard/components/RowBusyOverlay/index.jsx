import React from "react";
import "./style.css";

const RowBusyOverlay = ({ isVisible, state }) => {
  if (!isVisible) return null;

  return (
    <div className={`row__overlay row__overlay--${state}`}>
      <i className="fas fa-spinner fa-spin" />
    </div>
  );
};

export default RowBusyOverlay;
