import React, { Component } from "react";
import moment from "moment";
import RowBusyOverlay from "./../RowBusyOverlay";

class DoneRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBusy: false,
      overlayState: ""
    };

    this.handleOnUncomplete = this.handleOnUncomplete.bind(this);
  }

  handleOnUncomplete(projectId, taskId) {
    this.setState({ isBusy: true, overlayState: "uncompleting" });

    this.props.onUncomplete(projectId, taskId);
  }

  render() {
    const { projectId, taskId, description, title } = this.props;

    return (
      <div className="panel-block pc__done" title={moment(title).format("LLL")}>
        <input
          type="checkbox"
          checked
          onChange={() => this.handleOnUncomplete(projectId, taskId)}
        />
        <span>{description}</span>
        <time className="tag is-light">{moment(title).format("LLL")}</time>
        <RowBusyOverlay
          isVisible={this.state.isBusy}
          state={this.state.overlayState}
        />
      </div>
    );
  }
}

export default DoneRow;
