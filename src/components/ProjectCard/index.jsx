import React, { Component } from "react";
import CreateForm from "./components/CreateForm";
import TodoRow from "./components/TodoRow";
import DoneRow from "./components/DoneRow";
import "./style.css";

class ProjectCard extends Component {
  renderTodo() {
    return this.props.tasks
      .filter(t => !t.finishedAt)
      .map(t => (
        <TodoRow
          projectId={this.props._id}
          taskId={t._id}
          description={t.description}
          onComplete={this.props.onComplete}
          onDelete={this.props.onDeleteTask}
          key={t._id}
        />
      ));
  }

  renderDone() {
    return this.props.tasks
      .filter(t => t.finishedAt)
      .map(t => (
        <DoneRow
          projectId={this.props._id}
          taskId={t._id}
          title={t.finishedAt}
          description={t.description}
          onUncomplete={this.props.onUncomplete}
          key={t._id}
        />
      ));
  }

  render() {
    return (
      <nav className="panel pc__nav">
        <div className="panel-heading">
          <span className="panel-title">{this.props.name}</span>
          <div className="panel-options">
            <button
              className="button"
              onClick={() => this.props.onDelete(this.props._id)}
            >
              <i className="fas fa-trash" />
            </button>
            <button
              className="button"
              onClick={() => this.props.onEdit(this.props._id)}
            >
              <i className="fas fa-edit" />
            </button>
          </div>
        </div>
        <div className="panel-block todo">TODO</div>
        {this.renderTodo()}
        <div className="panel-block done">DONE</div>
        {this.renderDone()}
        <div className="panel-block form">
          <CreateForm projectId={this.props._id} />
        </div>
      </nav>
    );
  }
}

export default ProjectCard;
