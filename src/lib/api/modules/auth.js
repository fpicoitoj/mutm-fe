import API from "./../index";

/**
 * Game API helper which has all the necessary routes
 */
class AuthAPI extends API {
  constructor() {
    super();
    this.uri = "auth";
    this.requiresAuth = false;
  }

  async register({ name, email, password }) {
    const result = await this.apiPost(
      `${this.uri}/register`,
      { name, email, password },
      this.requiresAuth
    );

    return result;
  }

  async login({ email, password }) {
    const result = await this.apiPost(
      `${this.uri}/login`,
      { email, password },
      this.requiresAuth
    );

    if (!result) return null;

    const token = result.token;
    if (token) localStorage.setItem("token", token);

    return result;
  }

  async verify() {
    return await this.apiPost(`${this.uri}/verify`, {}, true);
  }
}

const api = new AuthAPI();
export default api;
