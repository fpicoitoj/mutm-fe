import API from "./../index";

/**
 * Project API helper which has all the necessary routes
 */
class ProjectAPI extends API {
  constructor() {
    super();
    this.uri = "projects";
    this.requiresAuth = true;
  }

  async getAll() {
    const result = await this.apiGet(`${this.uri}/all`, this.requiresAuth);
    return result;
  }

  async get(id) {
    const result = await this.apiGet(`${this.uri}/${id}`, this.requiresAuth);
    return result;
  }

  async create(payload) {
    const result = await this.apiPost(
      `${this.uri}`,
      payload,
      this.requiresAuth
    );
    return result;
  }

  async update(id, payload) {
    const result = await this.apiPut(
      `${this.uri}/${id}`,
      payload,
      this.requiresAuth
    );
    return result;
  }

  async delete(id) {
    const result = await this.apiDelete(`${this.uri}/${id}`, this.requiresAuth);
    return result;
  }

  async addTask(projectId, payload) {
    const result = await this.apiPost(
      `${this.uri}/${projectId}/tasks`,
      payload,
      this.requiresAuth
    );
    return result;
  }

  async deleteTask(projectId, id) {
    const result = await this.apiDelete(
      `${this.uri}/${projectId}/tasks/${id}`,
      this.requiresAuth
    );
    return result;
  }

  async completeTask(projectId, id) {
    const result = await this.apiPost(
      `${this.uri}/${projectId}/tasks/${id}/complete`,
      null,
      this.requiresAuth
    );
    return result;
  }

  async uncompleteTask(projectId, id) {
    const result = await this.apiPost(
      `${this.uri}/${projectId}/tasks/${id}/uncomplete`,
      null,
      this.requiresAuth
    );
    return result;
  }
}

const api = new ProjectAPI();
export default api;
