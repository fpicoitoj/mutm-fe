import API from "./../index";

/**
 * Task API helper which has all the necessary routes
 */
class TaskAPI extends API {
  constructor() {
    super();
    this.uri = "tasks";
    this.requiresAuth = true;
  }

  async getAll() {
    const result = await this.apiGet(`${this.uri}/all`, this.requiresAuth);
    return result;
  }

  async get(id) {
    const result = await this.apiGet(`${this.uri}/${id}`, this.requiresAuth);
    return result;
  }

  async create(id, payload) {
    const result = await this.apiPost(
      `${this.uri}`,
      payload,
      this.requiresAuth
    );
    return result;
  }

  async update(id, payload) {
    const result = await this.apiPut(
      `${this.uri}/${id}`,
      payload,
      this.requiresAuth
    );
    return result;
  }

  async delete(id) {
    const result = await this.apiDelete(`${this.uri}/${id}`, this.requiresAuth);
    return result;
  }
}

const api = new TaskAPI();
export default api;
