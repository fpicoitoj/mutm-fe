import axios from "axios";

/**
 * This is very simply a wrapper for Axios
 * All the APIs in the modules folder will extend this
 * and call upon its functions
 */
class API {
  constructor() {
    this.http = axios.create({
      baseURL: process.env.REACT_APP_API_URL
    });

    this.http.interceptors.response.use(r => r, this._handleError);
  }

  _getAuthorizationHeader() {
    return { Authorization: localStorage.token };
  }

  _handleError(error) {
    const { status } = error.response;

    if (status === 401) {
      localStorage.removeItem("token");
      window.location.href = "/";
    }

    return Promise.reject(error);
  }

  async _request(req, requiresAuth) {
    try {
      if (requiresAuth) {
        const authorization = this._getAuthorizationHeader();
        req = {
          ...req,
          headers: {
            ...req.headers,
            ...authorization
          }
        };
      }

      const { data } = await this.http(req);
      if (typeof data === "object") data.status = 200;

      return data;
    } catch (err) {
      return {
        status: 500,
        message: err.response.data.message
      };
    }
  }

  apiGet(url, requiresAuth = true, config = null) {
    return this._request(
      {
        method: "get",
        url,
        ...config
      },
      requiresAuth
    );
  }

  apiPost(url, payload, requiresAuth = true, config = null) {
    return this._request(
      {
        method: "post",
        url,
        data: payload,
        ...config
      },
      requiresAuth
    );
  }

  apiPut(url, payload, requiresAuth = true, config = null) {
    return this._request(
      {
        method: "put",
        url,
        data: payload,
        ...config
      },
      requiresAuth
    );
  }

  apiPatch(url, payload, requiresAuth = true, config = null) {
    return this._request(
      {
        method: "patch",
        url,
        data: payload,
        ...config
      },
      requiresAuth
    );
  }

  apiDelete(url, requiresAuth = true, config = null) {
    return this._request(
      {
        method: "delete",
        url,
        ...config
      },
      requiresAuth
    );
  }
}

export default API;
